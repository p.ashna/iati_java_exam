package ir.iati.exam.repository.api;

import ir.iati.exam.service.dto.WeatherDTO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Optional;

@Repository
public class WeatherRepository {

    private static final String URL = "http://api.weatherstack.com/current";
    private static final String ACCESS_KEY = "83b57f01850692934b596ef1047ce535";

    private final RestTemplate restTemplate;

    public WeatherRepository(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Optional<WeatherDTO> findByQuery(String query) {
        MultiValueMap<String, String> parts = new LinkedMultiValueMap<>();
        parts.add("access_key", ACCESS_KEY);
        parts.add("query", query);
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(URL);
        uriComponentsBuilder.queryParams(parts);
        ResponseEntity<WeatherDTO> response = restTemplate
                .exchange(
                        uriComponentsBuilder.build().toUri(),
                        HttpMethod.GET,
                        null,
                        WeatherDTO.class);
        return Optional.ofNullable(response.getBody());
    }
}
