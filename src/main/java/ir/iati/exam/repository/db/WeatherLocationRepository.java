package ir.iati.exam.repository.db;

import ir.iati.exam.entity.WeatherLocationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WeatherLocationRepository extends JpaRepository<WeatherLocationEntity, Long> {

    Optional<WeatherLocationEntity> findByCountryAndName(String country, String name);
}
