package ir.iati.exam.repository.db;

import ir.iati.exam.entity.WeatherLocationItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WeatherLocationItemRepository
        extends JpaRepository<WeatherLocationItemEntity, Long> {

    Optional<WeatherLocationItemEntity> findFirstByWeatherLocationNameOrderByCreationTimeDesc(String cityName);
}
