package ir.iati.exam.web.controller;

import ir.iati.exam.service.WeatherService;
import ir.iati.exam.service.dto.WeatherDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/exam/weather")
public class WeatherController {

    private final WeatherService weatherService;

    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }


    @GetMapping("/{cityName}")
    public ResponseEntity<WeatherDTO> findByCityName(@PathVariable("cityName") String cityName) {
        log.debug("REST Request to find weather by cityName : {}", cityName);
        WeatherDTO lastByCityName = weatherService.findLastByCityName(cityName);
        return new ResponseEntity<>(lastByCityName, HttpStatus.OK);
    }

    @GetMapping("/latest/{cityName}")
    public ResponseEntity<Page<WeatherDTO>> findLatestByCityName(@PathVariable("cityName") String cityName) {
        log.debug("REST Request to find latest weather by cityName : {}", cityName);
        Page<WeatherDTO> latestByCityName = weatherService.findLatestByCityName(cityName);
        return new ResponseEntity<>(latestByCityName, HttpStatus.OK);
    }
}
