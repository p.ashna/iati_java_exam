package ir.iati.exam.web.controller;

import ir.iati.exam.service.HotelService;
import ir.iati.exam.service.dto.HotelDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/exam/hotel")
public class HotelController {

    private final HotelService hotelService;

    public HotelController(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    @PostMapping("/set")
    public ResponseEntity<HotelDTO> save(@RequestBody HotelDTO hotelDTO) {
        log.debug("REST Request to save hotel : {}", hotelDTO);
        HotelDTO savedHotel = hotelService.save(hotelDTO);
        return new ResponseEntity<>(savedHotel, HttpStatus.OK);
    }

    @GetMapping("/all")
    public ResponseEntity<Page<HotelDTO>> findAll(Pageable pageable) {
        log.debug("REST Request to findAll hotels");
        Page<HotelDTO> all = hotelService.findAll(pageable);
        return new ResponseEntity<>(all, HttpStatus.OK);
    }
}
