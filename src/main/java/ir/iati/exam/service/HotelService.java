package ir.iati.exam.service;

import ir.iati.exam.service.dto.HotelDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface HotelService {

    HotelDTO save(HotelDTO hotelDTO);

    Page<HotelDTO> findAll(Pageable pageable);
}
