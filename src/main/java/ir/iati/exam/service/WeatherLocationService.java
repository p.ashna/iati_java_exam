package ir.iati.exam.service;

import ir.iati.exam.service.dto.WeatherLocationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface WeatherLocationService {

    WeatherLocationDTO save(WeatherLocationDTO weatherLocationDTO);

    Page<WeatherLocationDTO> findAll(Pageable pageable);

    Optional<WeatherLocationDTO> findById(Long id);

    Optional<WeatherLocationDTO> findByName(String country, String name);
}
