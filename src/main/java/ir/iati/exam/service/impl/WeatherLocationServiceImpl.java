package ir.iati.exam.service.impl;

import ir.iati.exam.entity.WeatherLocationEntity;
import ir.iati.exam.repository.db.WeatherLocationRepository;
import ir.iati.exam.service.WeatherLocationService;
import ir.iati.exam.service.dto.WeatherLocationDTO;
import ir.iati.exam.service.mapper.WeatherLocationMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WeatherLocationServiceImpl implements WeatherLocationService {

    private final WeatherLocationRepository weatherLocationRepository;
    private final WeatherLocationMapper weatherLocationMapper;

    public WeatherLocationServiceImpl(WeatherLocationRepository weatherLocationRepository,
                                      WeatherLocationMapper weatherLocationMapper) {
        this.weatherLocationRepository = weatherLocationRepository;
        this.weatherLocationMapper = weatherLocationMapper;
    }

    @Override
    public WeatherLocationDTO save(WeatherLocationDTO weatherLocationDTO) {
        WeatherLocationEntity weatherLocationEntity = weatherLocationMapper.toEntity(weatherLocationDTO);
        weatherLocationEntity = weatherLocationRepository.save(weatherLocationEntity);
        return weatherLocationMapper.toDTO(weatherLocationEntity);
    }

    @Override
    public Page<WeatherLocationDTO> findAll(Pageable pageable) {
        return weatherLocationRepository
                .findAll(pageable)
                .map(weatherLocationMapper::toDTO);
    }

    @Override
    public Optional<WeatherLocationDTO> findById(Long id) {
        return weatherLocationRepository
                .findById(id)
                .map(weatherLocationMapper::toDTO);
    }

    @Override
    public Optional<WeatherLocationDTO> findByName(String country, String name) {
        return weatherLocationRepository
                .findByCountryAndName(country, name)
                .map(weatherLocationMapper::toDTO);
    }
}
