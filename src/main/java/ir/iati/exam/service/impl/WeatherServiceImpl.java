package ir.iati.exam.service.impl;

import ir.iati.exam.repository.api.WeatherRepository;
import ir.iati.exam.service.WeatherLocationItemService;
import ir.iati.exam.service.WeatherLocationService;
import ir.iati.exam.service.WeatherService;
import ir.iati.exam.service.dto.WeatherDTO;
import ir.iati.exam.service.dto.WeatherLocationDTO;
import ir.iati.exam.service.dto.WeatherLocationItemDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class WeatherServiceImpl implements WeatherService {

    public static final int PAGE_SIZE = 5;

    private final WeatherRepository weatherRepository;
    private final WeatherLocationService weatherLocationService;
    private final WeatherLocationItemService weatherLocationItemService;


    public WeatherServiceImpl(WeatherRepository weatherRepository,
                              WeatherLocationService weatherLocationService,
                              WeatherLocationItemService weatherLocationItemService) {
        this.weatherRepository = weatherRepository;
        this.weatherLocationService = weatherLocationService;
        this.weatherLocationItemService = weatherLocationItemService;
    }

    @Override
    public void findFromApi(String query) {
        Optional<WeatherDTO> byQuery = weatherRepository.findByQuery(query);
        if (byQuery.isPresent()) {
            WeatherLocationDTO location = byQuery.get().getLocation();
            Optional<WeatherLocationDTO> weatherLocationByCountryAndName = weatherLocationService
                    .findByName(location.getCountry(), location.getName());
            Long weatherLocationId;
            if (weatherLocationByCountryAndName.isPresent()) {
                weatherLocationId = weatherLocationByCountryAndName.get().getId();
            } else {
                WeatherLocationDTO savedLocation = weatherLocationService.save(location);
                weatherLocationId = savedLocation.getId();
            }
            byQuery.get().getCurrent().setWeatherLocationId(weatherLocationId);
            this.weatherLocationItemService.save(byQuery.get().getCurrent());
        }
    }

    @Override
    public void findFromApi(String query, Long weatherLocationId) {
        Optional<WeatherDTO> byQuery = weatherRepository.findByQuery(query);
        if (byQuery.isPresent()) {
            WeatherDTO foundedWeather = byQuery.get();
            foundedWeather.getCurrent().setWeatherLocationId(weatherLocationId);
            this.weatherLocationItemService.save(foundedWeather.getCurrent());
        }
    }

    @Override
    @Scheduled(fixedDelay = 120000)
    public void findFromApi() {
        this.findWeatherLocationPageable(0);
    }

    private void findWeatherLocationPageable(int currentPage) {
        Pageable pageable = PageRequest.of(currentPage, PAGE_SIZE);
        Page<WeatherLocationDTO> all = weatherLocationService.findAll(pageable);
        for (WeatherLocationDTO weatherLocationDTO : all.getContent()) {
            this.findFromApi(weatherLocationDTO.getName(), weatherLocationDTO.getId());
        }
        if (!all.isLast()) {
            this.findWeatherLocationPageable(++currentPage);
        }
    }

    @Override
    public WeatherDTO findLastByCityName(String cityName) {
        Optional<WeatherLocationItemDTO> byCityName = weatherLocationItemService.findByCityName(cityName);
        if (byCityName.isPresent()) {
            WeatherDTO weatherDTO = new WeatherDTO();
            WeatherLocationItemDTO weatherLocationItemDTO = byCityName.get();
            weatherDTO.setCurrent(weatherLocationItemDTO);
            Optional<WeatherLocationDTO> weatherLocationById = weatherLocationService
                    .findById(weatherLocationItemDTO.getWeatherLocationId());
            weatherLocationById.ifPresent(weatherDTO::setLocation);
            return weatherDTO;
        } else {
            this.findFromApi(cityName);
            return this.findLastByCityName(cityName);
        }
    }

    @Override
    public Page<WeatherDTO> findLatestByCityName(String cityName) {
        List<WeatherDTO> weatherDTOS = new ArrayList<>();
        Page<WeatherLocationItemDTO> latestByCityName = weatherLocationItemService.findLatestByCityName(cityName);
        WeatherLocationDTO weatherLocationDTO = null;
        for (WeatherLocationItemDTO weatherLocationItemDTO : latestByCityName) {
            if (weatherLocationDTO == null) {
                Optional<WeatherLocationDTO> byId = weatherLocationService.findById(weatherLocationItemDTO.getWeatherLocationId());
                if (byId.isPresent()) {
                    weatherLocationDTO = byId.get();
                }
            }
            WeatherDTO weatherDTO = new WeatherDTO();
            weatherDTO.setLocation(weatherLocationDTO);
            weatherDTO.setCurrent(weatherLocationItemDTO);
            weatherDTOS.add(weatherDTO);
        }
        return new PageImpl<>(weatherDTOS);
    }
}
