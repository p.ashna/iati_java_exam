package ir.iati.exam.service.impl;

import ir.iati.exam.entity.HotelEntity;
import ir.iati.exam.repository.db.HotelRepository;
import ir.iati.exam.service.HotelService;
import ir.iati.exam.service.dto.HotelDTO;
import ir.iati.exam.service.mapper.HotelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class HotelServiceImpl implements HotelService {

    private final HotelRepository hotelRepository;
    private final HotelMapper hotelMapper;

    public HotelServiceImpl(HotelRepository hotelRepository,
                            HotelMapper hotelMapper) {
        this.hotelRepository = hotelRepository;
        this.hotelMapper = hotelMapper;
    }

    @Override
    public HotelDTO save(HotelDTO hotelDTO) {
        HotelEntity hotelEntity = hotelMapper.toEntity(hotelDTO);
        hotelEntity = hotelRepository.save(hotelEntity);
        return hotelMapper.toDTO(hotelEntity);
    }

    @Override
    public Page<HotelDTO> findAll(Pageable pageable) {
        Pageable page = PageRequest.of(
                pageable.getPageNumber(),
                pageable.getPageSize(),
                Sort.by(
                        new Sort.Order(Sort.Direction.ASC, "price"),
                        new Sort.Order(Sort.Direction.DESC, "stars")
                )
        );
        return hotelRepository
                .findAll(page)
                .map(hotelMapper::toDTO);
    }
}
