package ir.iati.exam.service.impl;

import ir.iati.exam.entity.WeatherLocationItemEntity;
import ir.iati.exam.repository.db.WeatherLocationItemRepository;
import ir.iati.exam.service.WeatherLocationItemService;
import ir.iati.exam.service.dto.WeatherLocationItemDTO;
import ir.iati.exam.service.mapper.WeatherLocationItemMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class WeatherLocationItemServiceImpl implements WeatherLocationItemService {

    private final WeatherLocationItemRepository weatherLocationItemRepository;
    private final WeatherLocationItemMapper weatherLocationItemMapper;

    public WeatherLocationItemServiceImpl(WeatherLocationItemRepository weatherLocationItemRepository,
                                          WeatherLocationItemMapper weatherLocationItemMapper) {
        this.weatherLocationItemRepository = weatherLocationItemRepository;
        this.weatherLocationItemMapper = weatherLocationItemMapper;
    }

    @Override
    public WeatherLocationItemDTO save(WeatherLocationItemDTO weatherLocationItemDTO) {
        if (weatherLocationItemDTO.getWeatherLocationId() != null) {
            WeatherLocationItemEntity weatherLocationItemEntity = weatherLocationItemMapper.toEntity(weatherLocationItemDTO);
            weatherLocationItemEntity.setCreationTime(new Date());
            weatherLocationItemEntity = weatherLocationItemRepository.save(weatherLocationItemEntity);
            return weatherLocationItemMapper.toDTO(weatherLocationItemEntity);
        }
        return null;
    }

    @Override
    public Optional<WeatherLocationItemDTO> findByCityName(String cityName) {
        return weatherLocationItemRepository
                .findFirstByWeatherLocationNameOrderByCreationTimeDesc(cityName)
                .map(weatherLocationItemMapper::toDTO);
    }

    @Override
    public Page<WeatherLocationItemDTO> findLatestByCityName(String cityName) {
        Pageable pageable = PageRequest.of(
                0,
                10,
                Sort.by(new Sort.Order(Sort.Direction.DESC, "creationTime"))
        );
        return weatherLocationItemRepository
                .findAll(pageable)
                .map(weatherLocationItemMapper::toDTO);
    }
}
