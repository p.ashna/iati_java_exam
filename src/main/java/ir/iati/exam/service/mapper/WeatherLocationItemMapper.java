package ir.iati.exam.service.mapper;

import ir.iati.exam.entity.WeatherLocationItemEntity;
import ir.iati.exam.service.dto.WeatherLocationItemDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {WeatherLocationMapper.class})
public interface WeatherLocationItemMapper
        extends BaseMapper<WeatherLocationItemEntity, WeatherLocationItemDTO> {

    @Override
    @Mapping(source = "weatherLocationId", target = "weatherLocation.id")
    WeatherLocationItemEntity toEntity(WeatherLocationItemDTO dto);

    @Override
    @Mapping(source = "weatherLocation.id", target = "weatherLocationId")
    WeatherLocationItemDTO toDTO(WeatherLocationItemEntity entity);
}
