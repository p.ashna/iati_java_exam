package ir.iati.exam.service.mapper;

import ir.iati.exam.entity.WeatherLocationEntity;
import ir.iati.exam.service.dto.WeatherLocationDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface WeatherLocationMapper
        extends BaseMapper<WeatherLocationEntity, WeatherLocationDTO> {
}
