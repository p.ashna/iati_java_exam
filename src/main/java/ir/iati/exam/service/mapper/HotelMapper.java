package ir.iati.exam.service.mapper;

import ir.iati.exam.entity.HotelEntity;
import ir.iati.exam.service.dto.HotelDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface HotelMapper extends BaseMapper<HotelEntity, HotelDTO> {
}
