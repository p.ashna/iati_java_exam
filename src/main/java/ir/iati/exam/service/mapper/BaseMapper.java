package ir.iati.exam.service.mapper;

import java.util.Set;

public interface BaseMapper<E, D> {

    E toEntity(D dto);

    D toDTO(E entity);

    Set<D> toDTO(Set<E> entities);
}
