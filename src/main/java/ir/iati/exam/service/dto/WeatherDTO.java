package ir.iati.exam.service.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class WeatherDTO implements Serializable {

    private Object request;
    private WeatherLocationDTO location;
    private WeatherLocationItemDTO current;
}
