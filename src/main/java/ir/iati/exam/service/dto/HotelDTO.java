package ir.iati.exam.service.dto;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@ToString
public class HotelDTO implements Serializable {

    private Long id;
    @NonNull
    private String name;
    @NonNull
    private int stars;
    @NonNull
    private double price;
    @NonNull
    private Set<String> attributes;
}
