package ir.iati.exam.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.swing.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@ToString
public class WeatherLocationItemDTO implements Serializable {
    @JsonProperty("observation_time")
    private String observationTime;
    private int temperature;
    @JsonProperty("weather_code")
    private int weatherCode;
    @JsonProperty("weather_icons")
    private Set<String> weatherIcons;
    @JsonProperty("weather_descriptions")
    private Set<String> weatherDescriptions;
    @JsonProperty("wind_speed")
    private int windSpeed;
    @JsonProperty("wind_degree")
    private int windDegree;
    @JsonProperty("wind_dir")
    private String windDirection;
    private int pressure;
    private int precip;
    private int humidity;
    @JsonProperty("cloudcover")
    private int cloudCover;
    @JsonProperty("feelslike")
    private int feelsLike;
    @JsonProperty("uv_index")
    private int uvIndex;
    private int visibility;
    @JsonProperty("is_day")
    private String day;
    @JsonIgnore
    private Long weatherLocationId;
}
