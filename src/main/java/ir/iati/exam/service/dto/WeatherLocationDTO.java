package ir.iati.exam.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class WeatherLocationDTO implements Serializable {
    @JsonIgnore
    private Long id;
    private String name;
    private String country;
    private String region;
    @JsonProperty("lat")
    private String latitude;
    @JsonProperty("lon")
    private String longitude;
    @JsonProperty("timezone_id")
    private String timezoneId;
}
