package ir.iati.exam.service;

import ir.iati.exam.service.dto.WeatherLocationItemDTO;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface WeatherLocationItemService {

    WeatherLocationItemDTO save(WeatherLocationItemDTO weatherLocationItemDTO);

    Optional<WeatherLocationItemDTO> findByCityName(String cityName);

    Page<WeatherLocationItemDTO> findLatestByCityName(String cityName);
}
