package ir.iati.exam.service;

import ir.iati.exam.service.dto.WeatherDTO;
import org.springframework.data.domain.Page;

public interface WeatherService {

    void findFromApi(String query);

    void findFromApi(String query, Long weatherLocationId);

    void findFromApi();

    WeatherDTO findLastByCityName(String cityName);

    Page<WeatherDTO> findLatestByCityName(String cityName);
}
