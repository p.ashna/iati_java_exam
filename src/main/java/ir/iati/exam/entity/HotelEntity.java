package ir.iati.exam.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@Table(name = "hotel")
public class HotelEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private int stars;
    private double price;
    @ElementCollection
    @CollectionTable(
            name = "hotel_attributes",
            joinColumns = {@JoinColumn(name = "hotel_id")}
    )
    @Column(name = "attribute")
    private Set<String> attributes;
}
