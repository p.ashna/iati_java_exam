package ir.iati.exam.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@Table(name = "weather_location_item")
public class WeatherLocationItemEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "observation_time")
    private String observationTime;
    private int temperature;
    @Column(name = "weather_code")
    private int weatherCode;
    @ElementCollection
    @CollectionTable(
            name = "weather_icon",
            joinColumns = {@JoinColumn(name = "ITEM_ID")}
    )
    @Column(name = "icon")
    private Set<String> weatherIcons;
    @ElementCollection
    @CollectionTable(
            name = "weather_description",
            joinColumns = {@JoinColumn(name = "ITEM_ID")}
    )
    @Column(name = "description")
    private Set<String> weatherDescriptions;
    @Column(name = "wind_speed")
    private int windSpeed;
    @Column(name = "wind_degree")
    private int windDegree;
    @Column(name = "wind_direction")
    private String windDirection;
    private int pressure;
    private float precip;
    private int humidity;
    @Column(name = "cloud_cover")
    private int cloudCover;
    @Column(name = "feels_like")
    private int feelsLike;
    @Column(name = "uv_index")
    private int uvIndex;
    private String visibility;
    private String day;
    @ManyToOne
    @JoinColumn(nullable = false)
    private WeatherLocationEntity weatherLocation;
    @Column(name = "creation_time")
    private Date creationTime;
}
