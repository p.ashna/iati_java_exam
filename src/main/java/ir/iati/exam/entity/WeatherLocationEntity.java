package ir.iati.exam.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString
@Entity
@Table(name = "weather_location")
public class WeatherLocationEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String country;
    private String region;
    private String latitude;
    private String longitude;
    @Column(name = "timezone_id")
    private String timezoneId;

}
